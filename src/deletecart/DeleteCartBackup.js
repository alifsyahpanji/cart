import React, { useState, useEffect } from 'react';
import ilusDeleteCart from './assets/ilusdeletecart.png';

function DeleteCart() {
    const [deleteCart, setDeleteCart] = useState(null);

    useEffect(() => {
        (async () => {
            const res = await fetch("https://dummyjson.com/carts/1", {
                method: 'DELETE',
            });
            const data = await res.json();
            setDeleteCart(data);

        })();
    })



    return (
        <div className="App d-flex justify-content-center">
            <div className="containercustom">



                {deleteCart && <div className="deletecart d-flex flex-column align-items-center mt-5">
                    <div><img src={ilusDeleteCart} alt="Delete Cart" /></div>
                    <div className='titledelete mt-3'>Delete Cart</div>
                    <div className='textdelete mt-3 mx-3 text-center'>Cart dengan ID {deleteCart.id} telah dihapus pada tanggal {deleteCart.deletedOn}</div>
                    <div className='mt-4'><a href="/"><button className='btnconfrimdelcart'>Oke</button></a></div>
                </div>
                }


                {!deleteCart && <div>
                    Loading ...
                </div>
                }


            </div>
        </div>
    );
}

export default DeleteCart;