import ilusDeleteCart from './assets/ilusdeletecart.png';
import React from 'react';

const deleteId = localStorage.getItem("deleteid");

class DeleteCart extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            items: []
        };
    }



    componentDidMount() {

        fetch("https://dummyjson.com/carts/" + deleteId, {
            method: 'DELETE',
        })
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        items: result
                    });
                },
                // Note: it's important to handle errors here
                // instead of a catch() block so that we don't swallow
                // exceptions from actual bugs in components.
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    render() {
        const { error, isLoaded, items } = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return (
                <div className="App d-flex justify-content-center">
                    <div className="containercustom d-flex justify-content-center align-items-center">
                        <div class="spinner-grow text-green" role="status">
                        </div>
                    </div>
                </div>
            );
        } else {
            return (
                <div className="App d-flex justify-content-center">
                    <div className="containercustom">



                        <div className="deletecart d-flex flex-column align-items-center mt-5">
                            <div><img src={ilusDeleteCart} alt="Delete Cart" /></div>
                            <div className='titledelete mt-3'>Delete Cart</div>
                            <div className='textdelete mt-3 mx-3 text-center'>Cart dengan ID {items.id} telah dihapus pada tanggal {items.deletedOn}</div>
                            <div className='mt-4'><a href="/"><button className='btnconfrimdelcart'>Oke</button></a></div>
                        </div>




                    </div>
                </div>
            );
        }
    }
}



export default DeleteCart;