function LoadSingleCart() {
    return (
        <div className="loadsinglecart mt-3 mx-3">
            <div className="shimmer-wrapper">
                <div className="shimmer"></div>
            </div>
        </div>
    );
}

export default LoadSingleCart;