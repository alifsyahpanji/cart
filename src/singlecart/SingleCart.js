import React, { useState, useEffect } from 'react';
import LoadSingleCart from './LoadSingleCart';
import buttonBackPng from './assets/iconarrow.png';
import decerment from './assets/buttonmin.png';
import incerment from './assets/buttonplus.png';
import iconedit from './assets/iconedit.png';



function SingleCart() {
    const [singleCart, setSingleCart] = useState(null);
    const [totalCart, setTotalCart] = useState(null);
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const id = urlParams.get('id');

    useEffect(() => {
        setTimeout(async () => {
            const res = await fetch("https://dummyjson.com/carts/" + id);
            const data = await res.json();
            setTotalCart(data);
            setSingleCart(data.products);

        }, 3000)
    })


    return (
        <div className="App d-flex justify-content-center">
            <div className="containercustom">

                <div className='singlecart'>

                    <div className='my-1'>
                        <a href='/'><span className='mx-2 cursorpointer'><img src={buttonBackPng} alt="button back" /></span></a> <span className='titlesinglecart'>Keranjang</span>
                    </div>

                    <div className='garisbesar my-3'></div>

                    <div className='d-flex'>
                        <div className='titlesinglecart mx-3 my-3'>Pesanan</div>
                        <div className='tambahpesanansinglecart ms-auto mx-3 my-3'><a href='/addcart'>+ Tambah Pesanan</a></div>

                    </div>
                    <div className='gariskecil mx-3'></div>

                    {singleCart && <div>
                        {singleCart.map(itemSingleCart => (
                            <div key={itemSingleCart.id} className='containerproduk mx-3 my-2'>
                                <div className='produkname'>{itemSingleCart.title}</div>

                                <div className='d-flex my-2'>
                                    <div className='produkharga me-auto'>${itemSingleCart.price}</div>

                                    <div className='produkjumlah'>
                                        <span className='mx-2'><img src={decerment} alt="decerment" /></span>
                                        <span className='produknumber mx-2'>{itemSingleCart.quantity}</span>
                                        <span className='mx-2'><img src={incerment} alt="incerment" /></span>
                                    </div>

                                </div>

                            </div>
                        ))}

                        <div className='d-flex mx-3 my-4'>
                            <div className='totalharga me-auto'>Total Harga</div>
                            <div className='nilaiharga'>${totalCart.total}</div>

                        </div>

                        <div className='garisbesar my-3'></div>

                        <div className='tambahkancatatan mx-3 my-3'>Tambahkan Catatan</div>

                        <div className='gariskecil mx-3'></div>

                        <div className='containercatatan mx-3 my-3 d-flex align-items-center'>
                            <span className='mx-3'><img src={iconedit} alt="icon edit" /></span>
                            <input className='inputcatatan form-control me-2' type="text" placeholder='Masukan catatan disini' />
                        </div>

                        <div className='garisbesar my-3'></div>

                        <div className='d-flex mx-3 my-3'>
                            <div className='totalharga me-auto'>Total Pembayaran</div>
                            <div className='nilaiharga'>${totalCart.total}</div>
                        </div>

                        <div className='d-flex justify-content-center my-2'><button type='submit' className='buttonpesanan'>Buat Pesanan</button></div>

                    </div>
                    }

                    {!singleCart && <div>
                        <LoadSingleCart />
                        <LoadSingleCart />
                        <LoadSingleCart />
                        <LoadSingleCart />
                        <LoadSingleCart />
                    </div>}












                </div>



            </div>
        </div>
    );
}

export default SingleCart;
