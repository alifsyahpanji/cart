import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import LoadAddCart from './LoadAddCart';
import buttonBackPng from './assets/iconarrow.png';
import buttonSearch from './assets/iconsearch.png';
import buttonFilter from './assets/iconfilter.png';


const handleClick = event => {

    localStorage.setItem("iditem", event.currentTarget.id);
    window.location.replace('/mycart');
};

function AddCart() {
    const [addCart, setAddCart] = useState(null);
    const navigate = useNavigate();


    useEffect(() => {
        setTimeout(async () => {
            const res = await fetch("https://dummyjson.com/products");
            const data = await res.json();
            setAddCart(data.products);
        }, 3000)
    })

    return (
        <div className="App d-flex justify-content-center">
            <div className="containercustom">

                <div className='addcart'>

                    <div className='my-1'>
                        <span onClick={() => navigate(-1)} className='mx-2 cursorpointer'><img src={buttonBackPng} alt="button back" /></span>
                        <span className='titlesinglecart'>Pencarian</span>
                    </div>

                    <div className='garisbesar my-3'></div>

                    <div className='containerpencarian d-flex justify-content-center'>

                        <div className='containertextfield d-flex align-items-center ms-3 me-auto'>
                            <div className='ms-2'><img src={buttonSearch} alt="button search" /></div>
                            <input className='inputcatatan form-control mx-2' type="text" placeholder='Mau cari apa' />

                        </div>

                        <div className='me-2'><img src={buttonFilter} alt="button filter" /></div>

                    </div>





                    {addCart && <div className='d-flex flex-wrap mx-3'>
                        {addCart.map(itemAddCart => (
                            <div key={itemAddCart.id} className='containeraddproduk mx-1 my-1'>
                                <div><img className='imgaddproduct' src={itemAddCart.thumbnail} alt={itemAddCart.title} /></div>

                                <div className='d-flex flex-column my-2'>
                                    <div className='titleaddproduk mx-1'>{itemAddCart.title}</div>
                                    <div className='priceaddproduk mx-1 my-2'>${itemAddCart.price}</div>
                                </div>

                                <div className='d-flex justify-content-center'>
                                    <button id={itemAddCart.id} className='buttonaddcart' onClick={handleClick}>Tambah</button>
                                </div>
                            </div>
                        ))}
                    </div>
                    }


                    {!addCart && <div>
                        <LoadAddCart />


                    </div>
                    }





                </div>


            </div>
        </div>
    );
}

export default AddCart;