import React, { useState, useEffect } from 'react';
import label from './assets/label.png';
import LoadAllCart from './LoadAllCart';
import { ReactComponent as Logo } from './assets/trash.svg';

const handleClick = event => {

    localStorage.setItem("deleteid", event.currentTarget.id);
    window.location.replace('/deletecart');
};

function AllCart() {
    const [allCart, setAllCart] = useState(null);
    const paramPath = "/singlecart?id=";


    useEffect(() => {
        setTimeout(async () => {
            const res = await fetch("https://dummyjson.com/carts");
            const data = await res.json();
            setAllCart(data.carts);
        }, 3000)
    })



    return (
        <div className="allcart">
            <div className="textallcart mx-2 my-2">All Cart</div>

            <div className="allcartdata mx-3 my-4">

                {allCart && allCart.map(itemAllCart => (
                    <div key={itemAllCart.id} className="cardallcartdata mt-3">

                        <div className="d-flex">
                            <div className='px-2 py-2 me-auto'><img src={label} alt="label" /></div>
                            <div className='px-2 py-2'><a href={paramPath + itemAllCart.id}><button className='btnallcard p-1'>View Cart</button></a></div>
                        </div>

                        <div className='d-flex justify-content-center my-2'>
                            <div className='gariscartdata'></div>
                        </div>

                        <div className='d-flex'>

                            <div className='containerinfocart'>
                                <div className='totalproduk px-3 py-1'>Total pesanan {itemAllCart.totalProducts} produk</div>

                                <div className='totalharga px-3 py-1'>${itemAllCart.total}</div>

                                <div className='cartid px-3'>ID Cart: {itemAllCart.id}</div>
                            </div>

                            <div className='containerdeletecartbtn d-flex justify-content-center align-items-center'>
                                <div id={itemAllCart.id} className='cursorpointer' onClick={handleClick}><Logo /></div>

                            </div>

                        </div>

                    </div>
                ))}

                {!allCart && <div>
                    <LoadAllCart />
                    <LoadAllCart />
                    <LoadAllCart />
                    <LoadAllCart />
                    <LoadAllCart />

                </div>
                }



            </div>
        </div>
    );
}

export default AllCart;