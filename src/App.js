import NavBottom from './NavBottom.js';
import ExtraNav from './ExtraNav.js';
import AllCart from './AllCart.js';

function App() {
  return (
    <div className="App d-flex justify-content-center">
      <div className="containercustom">
        <AllCart />

        <ExtraNav />
        <NavBottom />
      </div>
    </div>
  );
}

export default App;
