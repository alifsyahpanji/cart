import React, { useState, useEffect } from 'react';
import LoadSingleCart from '../singlecart/LoadSingleCart';
import buttonBackPng from './assets/iconarrow.png';
import decerment from './assets/buttonmin.png';
import incerment from './assets/buttonplus.png';
import iconedit from './assets/iconedit.png';




function MyCart() {
    const [myCart, setMyCart] = useState(null);
    let [num, setNum] = useState(1);
    let [totalPrice, setTotalPrice] = useState(0);
    const getIdItem = localStorage.getItem("iditem");





    let incNum = () => {
        let quantity = Number(num) + 1;
        setNum(quantity);
        countTotalPrice(quantity);
    }

    let decNum = () => {
        if (num > 1) {
            let quantity = Number(num) - 1;
            setNum(quantity);
            countTotalPrice(quantity);
        }
    }

    function countTotalPrice(qty = 1) {
        let totalPrice = 0;
        for (let index = 0; index < myCart.length; index++) {
            totalPrice = totalPrice + (myCart[index].price * qty);
        }
        setTotalPrice(totalPrice);
    }


    useEffect(() => {
        setTimeout(async () => {
            const res = await fetch("https://dummyjson.com/carts/add", {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({
                    userId: 1,
                    products: [
                        {
                            id: getIdItem,
                            quantity: 1,
                        },
                    ]
                })
            });
            const data = await res.json();
            setMyCart(data.products);
            countTotalPrice(num);

        }, 3000)
    })


    return (
        <div className="App d-flex justify-content-center">
            <div className="containercustom">

                <div className='singlecart'>

                    <div className='my-1'>
                        <a href='/'><span className='mx-2 cursorpointer'><img src={buttonBackPng} alt="button back" /></span></a> <span className='titlesinglecart'>Keranjang</span>
                    </div>

                    <div className='garisbesar my-3'></div>

                    <div className='d-flex'>
                        <div className='titlesinglecart mx-3 my-3'>Pesanan</div>
                        <div className='tambahpesanansinglecart ms-auto mx-3 my-3'><a href='/addcart'>+ Tambah Pesanan</a></div>

                    </div>
                    <div className='gariskecil mx-3'></div>

                    {myCart && <div>
                        {myCart.map(itemMyCart => (
                            <div key={itemMyCart.id} className='containerproduk mx-3 my-2'>
                                <div className='produkname'>{itemMyCart.title}</div>

                                <div className='d-flex my-2'>
                                    <div className='produkharga me-auto'>${itemMyCart.price}</div>

                                    <div className='produkjumlah'>
                                        <span className='mx-2'><img src={decerment} alt="decerment" onClick={decNum} /></span>
                                        <span className='produknumber mx-2'>{num}</span>
                                        <span className='mx-2'><img src={incerment} alt="incerment" onClick={incNum} /></span>
                                    </div>

                                </div>

                            </div>
                        ))}

                        <div className='d-flex mx-3 my-4'>
                            <div className='totalharga me-auto'>Total Harga</div>
                            <div className='nilaiharga'>${totalPrice}</div>

                        </div>

                        <div className='garisbesar my-3'></div>

                        <div className='tambahkancatatan mx-3 my-3'>Tambahkan Catatan</div>

                        <div className='gariskecil mx-3'></div>

                        <div className='containercatatan mx-3 my-3 d-flex align-items-center'>
                            <span className='mx-3'><img src={iconedit} alt="icon edit" /></span>
                            <input className='inputcatatan form-control me-2' type="text" placeholder='Masukan catatan disini' />
                        </div>

                        <div className='garisbesar my-3'></div>

                        <div className='d-flex mx-3 my-3'>
                            <div className='totalharga me-auto'>Total Pembayaran</div>
                            <div className='nilaiharga'>${totalPrice}</div>
                        </div>

                        <div className='d-flex justify-content-center my-2'><button type='submit' className='buttonpesanan'>Buat Pesanan</button></div>

                    </div>
                    }

                    {!myCart && <div>
                        <LoadSingleCart />
                        <LoadSingleCart />
                        <LoadSingleCart />
                        <LoadSingleCart />
                        <LoadSingleCart />
                    </div>}












                </div>



            </div>
        </div>
    );
}

export default MyCart;
