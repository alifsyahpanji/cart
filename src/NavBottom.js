import beranda from './assets/beranda.png';
import bantuan from './assets/bantuan.png';
import transaksi from './assets/transaksi.png';
import notifikasi from './assets/notifikasi.png';
import profil from './assets/profil.png';

function NavBottom() {
    return (
        <div className="d-flex justify-content-center align-items-center bottomnav">
            <div className="mx-auto"><img src={beranda} alt="beranda" /></div>
            <div className="mx-auto"><img src={bantuan} alt="bantuan" /></div>
            <div className="mx-auto"><img src={transaksi} alt="transaksi" /></div>
            <div className="mx-auto"><img src={notifikasi} alt="notifikasi" /></div>
            <div className="mx-auto"><img src={profil} alt="profil" /></div>
        </div>
    );
}

export default NavBottom;