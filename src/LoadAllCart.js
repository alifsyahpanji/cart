function LoadAllCart() {

    return (
        <div className="loadallcart mt-3">
            <div className="shimmer-wrapper">
                <div className="shimmer"></div>
            </div>
        </div>
    );
}

export default LoadAllCart;