import React from 'react';
import ReactDOM from 'react-dom/client';
import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";
import './style.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import App from './App';
import SingleCart from './singlecart/SingleCart';
import AddCart from './addcart/AddCart';
import MyCart from './mycart/MyCart';
import DeleteCart from './deletecart/DeleteCart';
import reportWebVitals from './reportWebVitals';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<App />} />
        <Route path="singlecart" element={<SingleCart />} />
        <Route path="addcart" element={<AddCart />} />
        <Route path="mycart" element={<MyCart />} />
        <Route path="deletecart" element={<DeleteCart />} />

      </Routes>
    </BrowserRouter>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
